var generation;

function preload() {
}

function setup() {
	createCanvas(windowWidth-90, windowHeight);
	frameRate(60);
}

var title_height = 25;
var f = function(x, y, phi) {
	fill(0);
	ellipseMode(CENTER);
	translate(x*width/3, y*height/3);
	rotate(phi + frameCount*0.05);
	ellipse(0, 0, 150, 5);
	rotate(-(phi + frameCount*0.05));
	translate(-x*width/3, -y*height/3);
};

function draw() {
	background(255);
	fill(0);
	textSize(12);
	textAlign(CENTER);
	textFont('Georgia');

	text('Generation', width/2,10);
	text(frameCount, width/2,20);

	f(1, 1, 10);
	f(1, 1, 20);
	f(1, 1, 30);
	f(1, 1, 40);
	f(1, 1, 50);
	f(2, 1, 20);
	f(1, 2, 30);
	f(2, 2, 40);
}
