var baelle = [];
var inp;

function preload() {
	inp = loadJSON("./assets/test.json");
}

function setup() {
	createCanvas(windowWidth, windowHeight);
	frameRate(60);
	for (var j in inp)
	{
		j = inp[j];
		var ball = new ballObj(j.x, j.y, j.radius, j.bg);

		baelle.push(ball);
	}
}

function draw() {
	background(255);

	for (var i=0; i< baelle.length; i++) {
		baelle[i].disp();
		baelle[i].vectorInfo();
		for (var j = i+1; j<baelle.length; j++) {
			baelle[i].collide(baelle[j]);
		}
	}
}

function ballObj(x, y, radius, bg) {
	// Bewegung: zufällige Geschwindigkeit...
	this.speed = random(3, 7);
	// in eine zufällige Richtung
	this.angle = random(0, 360);
	// Koordinaten, Radius und Hintergrundfarbe aus der importierten JSON-Datei
	this.x = x;
	this.y = y;
	this.radius = radius;
	this.bg = bg;

	// Bei Mouseover auf den Bällen wird ein Vektor mit einer Größe proportional zur Länge des
	// Verschiebungsvektors eingeblendet.
	this.vectorInfo = function() {
		if ( collidePointCircle(mouseX, mouseY, this.x, this.y, this.radius) ) {
			// neuer Zeichenkontext:
			push();
			// auf Ball-Mittelpunkt zentrieren
			translate(this.x, this.y);
			fill("black");
			textAlign(CENTER);
			textSize(15);
			// Formatierter Output des momentanen Winkels
			text(""+(this.angle%360).toFixed(2)+"°", 0,100);
			fill(255);
			// Koordinatensystem drehen
			rotate(this.angle*(PI/180));
			// Darstellung des Vektors festlegen
			stroke(this.bg);
			strokeWeight(this.speed);

			// Länge des Vektor festlegen
			var len = this.deltaV().mag()*(this.radius/this.speed);
			
			// Vektor zeichnen:
			beginShape();
			vertex(0, 0);
			vertex(len, 0);
			vertex(len*0.9, len*0.1);
			vertex(len*0.9, -len*0.1);
			vertex(len*0.9, 0);
			endShape();
			pop();
			// Neuer Zeichenkontext beendet. Translation und Rotation zurückgesetzt.
		}
	};

	this.disp = function() {
		// Kollision mit den waagerechten Displaykanten
		if ((collideLineCircle(0,0,width,0,this.x,this.y,this.radius) ||
		 collideLineCircle(0,height,width,height,this.x,this.y,this.radius)) &&
		 p5.Vector.dot(createVector(this.x, this.y), this.deltaV()) != 0) {
			this.angle = 360 - this.angle;
		}
		
		// Kollision mit den senkrechten Displaykanten
		if (collideLineCircle(0,0,0,height,this.x,this.y,this.radius) ||
		 collideLineCircle(width,0,width,height,this.x,this.y,this.radius) &&
		 p5.Vector.dot(createVector(this.x, this.y), this.deltaV()) != 0) {
			
			this.angle = 180-this.angle;
		}

		noStroke();
		fill(this.bg);
		// Bewegen der Ball-Koordinaten in geradliniger Richtung
		this.x += this.deltaX();
		this.y += this.deltaY();
		// Zeichnen des Balls
		ellipse(this.x, this.y, this.radius);
	};

	this.collide = function(other) {
		// Kollidiert mit einem anderen Ball?
		if (collideCircleCircle(this.x, this.y, this.radius, other.x, other.y, other.radius) &&
			// Die nachfolgende Expression berechnet, ob sich die Bälle aufeinander zu bewegen:
			p5.Vector.dot(p5.Vector.sub(other.deltaV(), this.deltaV()), createVector(this.x-other.x, this.y-other.y)) > 0) {
			// Tangente für das voneinander Abprallen der Bälle berechnen:
			// Quelle: http://flatredball.com/documentation/tutorials/math/circle-collision/
			var tangentY = -(other.x-this.x);
			var tangentX = other.y -this.y;
			
			var tangent = createVector(tangentX, tangentY);
			tangent = tangent.normalize();

			var relativeVelocity = createVector(this.deltaX() - other.deltaX(),
				this.deltaY() - other.deltaY());
			var len = relativeVelocity.dot(tangent);
			var velocityComponentOnTangent = p5.Vector.mult(tangent,len);
			var velocityComponentPerpendicularToTangent = p5.Vector.sub(relativeVelocity, velocityComponentOnTangent);
			var other_deltaX = other.deltaX();
			var other_deltaY = other.deltaY();
			var deltaX_ = this.deltaX();
			var deltaY_ = this.deltaY();
			deltaX_ -= velocityComponentPerpendicularToTangent.x;
			deltaY_ -= velocityComponentPerpendicularToTangent.y;
			other_deltaX += velocityComponentPerpendicularToTangent.x;
			other_deltaY += velocityComponentPerpendicularToTangent.y;
			this.angle = atan2(deltaY_, deltaX_)*(180/PI);
			other.angle = atan2(other_deltaY, other_deltaX)*(180/PI);
		}
	};

	// x-Komponente der simulierten Bewegung, Umwandlung von Polar- in kartesische Koordinaten
	this.deltaX = function() {
		return this.speed * cos(this.angle*(PI/180));
	};

	// y-Komponente der simulierten Bewegung, Umwandlung von Polar- in kartesische Koordinaten
	this.deltaY = function() {
		return this.speed * sin(this.angle*(PI/180));
	};

	// kompletter Verschiebungsvektor der simulierten Bewegung, mittels Delegieren an andere Methoden
	this.deltaV = function() {
		return createVector(this.deltaX(), this.deltaY());
	};
}
